```
Jobs
 - 构成gitlab基本管道的每一项作业被称为job， job定义了做什么, 包含了很多东西


before_script、script、after_script 关键字可以指定当前job执行的脚本命令
 
```
###### Jobs 中的关键字 before_script、script after_script


```yml
# 以下是3个jobs的配置
run_test:
    before-script:
        - echo "before-script !!!"
        - npm run lint
    script:
        - npm run test

    after_script:
        - echo "clean something !!!"

build_image:
    script:
        - echo "Building image"

push_image:
    script:
        -echo "push_image"

```


```
    Pipeline 由多个Job组成的流水线, 有commit会自动触发gitlab Pipeline 自动构建
```

``` yml
# Stages 为多个Jobs分组，有默认执行顺序，默认的Stages为 Test， 当前Stages下的所有Job成功后才会执行下一个Stage, 当前Stages下的某个Job失败就不会执行下一个Stage, 同一个Stage下的Job如果失败，后续Job也会执行 [因为同一Stages下的Job是并行运行 ]

stages:
#   - 可自定义多个名称
    - test
    - build
    - deploy
```


```yml
    # 同一Stages下的Job是并行运行, 如果需要指定先后执行顺序， 需要needs关键字指定依赖关系， needs后指定一个或多个job的名称

test-job:
    stage: Test
    tags:
        - aliyun
        - ecs
        - remote

    before_script:
            - echo "before_script test-job -- 2"
    script:
            - wrong-command "错误命令测试"
            - echo "script test-job -- 2"
    after_script:
            - echo "after_script test-job -- 2"

    test-unit:
        stage: Test
        needs: 
            - test-job
        tags:
            - aliyun
            - ecs
            - remote
    
        before_script:
            - echo "before_script test-job -- 2"
            - whoami
        script:
            - echo "script test-job -- 2"
        after_script:
            - echo "after_script test-job -- 2"
```


```yml
    # 依赖外部脚本
# ...
before_script:
    # 较多的脚本降低阅读性，可在外部文件中维护脚本， 直接引用本地的文件路劲即可
    # 需要先添加执行权限
    - chmod +x prepare-test.sh
    - ./prepare-test.sh
# ...
```


```yml
    # 区分分支，针对不同的分支触发构建， 关键字 only/except 指定分支执行某些jobs
    only:
        - main
        - develop
```

```yml
    # workflow rules
    # $CI_COMMIT_BRANCH gitlab 变量 当前提交的分支
    workflow:
    rules:
        - if: $CI_COMMIT_BRANCH != "main" && $CI_PIPELINE_SOURCE != "merge_request_event"
        when: never
        - when: always
```

``` yml
  # gitlab 默认变量 https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
    # 可以全局定义变量， 也可以在job中定义
variables:
  DC_APP: docker.io/my-docker-id/my-app
  DC_VERSION: v1.9
```

```
    docker、shell 执行器, 如果没安装docker, 即使指定了image镜像，会被shell执行器忽略
    
    1. 安装 docker ubuntu:
        - apt  install docker.io
    2. 当前用户加入docker group
        - sudo usermod -aG docker $USER

    可全局指定image， 也可以在单独的job中指定image
```

```yml
# demo1 示例
    workflow:
  rules:
    - if: $CI_COMMIT_BRANCH != "main" && $CI_PIPELINE_SOURCE != "merge_request_event"
      when: never
    - when: always
  
# 全局变量
variables:
  DC_APP: docker.io/my-docker-id/my-app
  DC_VERSION: v1.9

stages:
  - Test
  - Build
  - Deploy

test-lint:
  stage: Test
  tags:
    - docker-runner
  image: node:alpine3.16

  script:
    - echo "test-lint"
    - echo "$CI_TEST_VARIBLE"
    - echo "$CI_TEST_VARIBLE_FILE"
    - cat $CI_TEST_VARIBLE_FILE
    - npm -v
    - node -v
  after_script:
    - echo "test-lint"

test-unit:
  stage: Test
  tags:
    - docker-runner
  image: node:14
  needs:
   - test-lint
  script:
    - echo "test-unit"
    - npm -v
    - node -v
build-images:
  stage: Build
  tags:
    - aliyun
    - ecs
    - remote
  only:
    - main
  
  script:
    - echo "build-images"

push-images:
  stage: Build
  tags:
    - aliyun
    - ecs
    - remote
  needs:
    - build-images
  only:
    - main
  
  script:
    - echo "push-images docker.io/my-docker-id/my-app:v1.9"
    - echo "push-images $DC_APP:$DC_VERSION"


deploy-job:
  stage: Deploy
  tags:
    - aliyun
    - ecs
    - remote
  only:
    - main
  script:
    - echo "deploy-job docker.io/my-docker-id/my-app:v1.9"
    - echo "deploy-job $DC_APP:$DC_VERSION"

```


```yml
# artifacts 产出物 https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html
# artifacts
    # - reports [测试报告、代码检测报告等]
    # artifacts 会自动提前分发给下一个stage中的job
    # 同一stage, 不同job是拿不到artifacts, 需要借助于关键字 dependencies
    dependencies:
      - job_name
```


```yml
# cache 缓存
    # cache 缓存设置

    # 1. key 相当于键名, paths 指定当前缓存的目录, 提高了编译速度, 对于docker执行器，需要额外配置etc/gitlab-s/config.toml文件,因为docker执行完毕后容器回收了，所以需要在容器外面指定volumn用来存储缓存,需要额外配置 cache_dir = "/cache"   volumes = ["/cache"] 【后面的volumes已存在】
    # 2. policy: pull-push 默认值 pull-push， 第一次是没有任何缓存，但会生成缓存并上传到gitrunner机器上, 后续使用缓存需要保证缓存的唯一性，只读性，所以使用缓存的地方 policy: pull
  
  # 在某个job中【创建】缓存
  cache:
    key: $CI_JOB_NAME
    paths:
        - app/node_modules
    policy: pull-push

    # Creating cache unit_test-protected...
    #     app/node_modules: found 8644 matching files and directories 
    #     No URL provided, cache will not be uploaded to shared cache server. Cache will be stored only locally. 
    #     https://gitlab.com/my-awesome-group812/node-project/-/jobs/3146426241

  # 在某个job中【使用】缓存
  # 指定缓存， policy 只能用于pull, 限制其push, 这样就保证了缓存的单一性 
  cache:
    key: $CI_JOB_NAME
    paths:
        - app/node_modules
    policy: pull
```

###### 清除cache缓存
```
  gitlab 提供了对外的清空缓存的功能， 但仅仅是清除当前最新的缓存，旧的缓存依旧在机器磁盘中，需要手动删除, 每次新创建缓存会在旧的缓存后有index值++
  Each time you clear the cache manually, the internal cache name is updated. The name uses the format cache-<index>, and the index increments by one. The old cache is not deleted. You can manually delete these files from the runner storage.

  Where the caches are stored
  
  docker-runner 缓存位置
    /var/lib/docker/volumes/<volume-id>/_data/<user>/<project>/<cache-key>/cache.zip.
    查询方法：
  1. docker volume ls
  2. docker volume inspect VOLUME NAME
     - MountPoint 挂载节点

  shell
    /home/gitlab-runner/cache/<user>/<project>/<cache-key>/cache.zip.
```


```yml
#  include 关键字，引入第三方、外部、本地、其他gitlab库 构建模板

# sast 关键字 指定当前SAST构建的stage
# Can run untagged jobs 需要保持 开启状态
sast:
  stage: Test

include:
  - template: Jobs/SAST.gitlab-ci.yml


# project 指定了配置库的所在组/项目名称
include:
  - project: fe/gitlab-ci-config
    ref: master
    file: /gitlab-ci-web.yml
```


```yml
  # 关键字 extends 优化结构, 用于抽离重复逻辑
  
  # 影子job， 在普通job名字前添加. 即可， gitlab-runner 扫描到影子节点， 不会被执行，在关键字extends后指定的名字也应该使用 .job_name
.deploy:
  tags: 
    - shell-runner
  variables:
    COMPOSE_PROJECT_NAME: ""
    DC_APP_PORT: ""
    ENVIRONMENT_NAME: ""
    ENVIRONMENT_POINT: ""
  before_script:
    - chmod 400 $SSH_KEY #变量
  script:
    - scp -o StrictHostKeyChecking=no -i $SSH_KEY ./docker-compose.yaml root@$SERVE_HOST:/home/root

    - ssh -o StrictHostKeyChecking=no -i $SSH_KEY root@$SERVE_HOST "
      cd /home/root &&
      docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD registry.gitlab.com &&
      
      export COMPOSE_PROJECT_NAME=$COMPOSE_PROJECT_NAME  &&
      export DC_APP_PORT=$DC_APP_PORT && 

      docker-compose -f docker-compose.yaml down &&
      docker-compose -f docker-compose.yaml up -d"
  environment:
    name: $ENVIRONMENT_NAME
    url: $ENVIRONMENT_POINT

# 部署dev 环境
deploy_dev:
  extends: .deploy
  stage: Deploy-Dev

  variables: 
    SERVE_HOST: $DEV_HOST
    COMPOSE_PROJECT_NAME: dev
    DC_APP_PORT: 3000
    ENVIRONMENT_NAME: development
    ENVIRONMENT_POINT: $DEVELOPMENT_END_POINT
  needs:
    - run_functional_test

# 部署staging 环境
deploy_dev:
  extends: .deploy
  stage: Deploy-Staging

  variables: 
    SERVE_HOST: $STAGING_HOST
    COMPOSE_PROJECT_NAME: staging
    DC_APP_PORT: 4000
    ENVIRONMENT_NAME: staging
    ENVIRONMENT_POINT: $STAGINGMENT_END_POINT

```